Prefix: obo: <http://purl.obolibrary.org/obo/>
Prefix: owl: <http://www.w3.org/2002/07/owl#>
Prefix: rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix: rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix: xml: <http://www.w3.org/XML/1998/namespace>
Prefix: xsd: <http://www.w3.org/2001/XMLSchema#>


Ontology: <http://www.semanticweb.org/stuckyb/ontologies/2018/5/obs_model_1>


ObjectProperty: obo:associated_with
ObjectProperty: obo:feeding_on
    SubPropertyOf: obo:associated_with
ObjectProperty: obo:ovipositing_on
    SubPropertyOf: obo:associated_with
ObjectProperty: obo:has_subject
ObjectProperty: obo:has_object


AnnotationProperty: obo:has_relation


Class: obo:Observation
Class: obo:Plant
Class: obo:Cicada
Class: obo:Wasp


Individual: obo:plant_1
    Types:
        obo:Plant


Individual: obo:cicada_1
    Types:
        obo:Cicada


Individual: obo:wasp_1
    Types:
        obo:Wasp


Individual: obo:observation_1

    Types:
        obo:Observation

    Annotations:
        obo:has_relation obo:feeding_on

    Facts:
        obo:has_object  obo:plant_1,
        obo:has_subject  obo:cicada_1


Individual: obo:observation_2

    Types:
        obo:Observation

    Annotations:
        obo:has_relation obo:ovipositing_on

    Facts:
        obo:has_object  obo:plant_1,
        obo:has_subject  obo:cicada_1


Individual: obo:observation_3

    Types:
        obo:Observation

    Annotations:
        obo:has_relation obo:feeding_on

    Facts:
        obo:has_object  obo:cicada_1,
        obo:has_subject  obo:wasp_1

