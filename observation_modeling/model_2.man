Prefix: obo: <http://purl.obolibrary.org/obo/>
Prefix: owl: <http://www.w3.org/2002/07/owl#>
Prefix: rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix: rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix: xml: <http://www.w3.org/XML/1998/namespace>
Prefix: xsd: <http://www.w3.org/2001/XMLSchema#>


Ontology: <http://www.semanticweb.org/stuckyb/ontologies/2018/5/obs_model_2>


ObjectProperty: obo:has_datum
ObjectProperty: obo:has_subject
ObjectProperty: obo:has_object


Class: obo:Observation
Class: obo:Plant
Class: obo:Cicada
Class: obo:Wasp

Class: obo:Associating
Class: obo:Feeding
    SubClassOf: obo:Associating
Class: obo:Ovipositing
    SubClassOf: obo:Associating


Individual: obo:plant_1
    Types:
        obo:Plant


Individual: obo:cicada_1
    Types:
        obo:Cicada


Individual: obo:wasp_1
    Types:
        obo:Wasp


Individual: obo:feeding_1
    Types:
        obo:Feeding

    Facts:
        obo:has_subject  obo:cicada_1,
        obo:has_object  obo:plant_1


Individual: obo:ovipos_1
    Types:
        obo:Ovipositing

    Facts:
        obo:has_subject  obo:cicada_1,
        obo:has_object  obo:plant_1


Individual: obo:feeding_2
    Types:
        obo:Feeding

    Facts:
        obo:has_subject  obo:wasp_1,
        obo:has_object  obo:cicada_1


Individual: obo:observation_1
    Types:
        obo:Observation

    Facts:
        obo:has_datum  obo:feeding_1


Individual: obo:observation_2
    Types:
        obo:Observation

    Facts:
        obo:has_datum  obo:ovipos_1


Individual: obo:observation_3
    Types:
        obo:Observation

    Facts:
        obo:has_datum  obo:feeding_2

