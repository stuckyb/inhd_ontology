Prefix: obo: <http://purl.obolibrary.org/obo/>
Prefix: owl: <http://www.w3.org/2002/07/owl#>
Prefix: rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix: rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix: xml: <http://www.w3.org/XML/1998/namespace>
Prefix: xsd: <http://www.w3.org/2001/XMLSchema#>


Ontology: <http://www.semanticweb.org/stuckyb/ontologies/2018/5/obs_model_1>


ObjectProperty: obo:associated_with
ObjectProperty: obo:feeding_on
    SubPropertyOf: obo:associated_with
ObjectProperty: obo:ovipositing_on
    SubPropertyOf: obo:associated_with
ObjectProperty: obo:about


Class: obo:Observation
Class: obo:Plant
Class: obo:Cicada
Class: obo:Wasp


Individual: obo:plant_1
    Types:
        obo:Plant


Individual: obo:cicada_1
    Types:
        obo:Cicada

    Facts:
        obo:feeding_on  obo:plant_1,
        obo:ovipositing_on  obo:plant_1


Individual: obo:wasp_1
    Types:
        obo:Wasp

    Facts:
        obo:feeding_on  obo:cicada_1


Individual: obo:observation_1

    Types:
        obo:Observation

    Facts:
        obo:about  obo:plant_1,
        obo:about  obo:cicada_1


Individual: obo:observation_2

    Types:
        obo:Observation

    Facts:
        obo:about  obo:plant_1,
        obo:about  obo:cicada_1


Individual: obo:observation_3

    Types:
        obo:Observation

    Facts:
        obo:about  obo:cicada_1,
        obo:about  obo:wasp_1

