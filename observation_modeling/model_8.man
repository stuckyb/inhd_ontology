Prefix: obo: <http://purl.obolibrary.org/obo/>
Prefix: owl: <http://www.w3.org/2002/07/owl#>
Prefix: rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
Prefix: rdfs: <http://www.w3.org/2000/01/rdf-schema#>
Prefix: xml: <http://www.w3.org/XML/1998/namespace>
Prefix: xsd: <http://www.w3.org/2001/XMLSchema#>


Ontology: <http://www.semanticweb.org/stuckyb/ontologies/2018/5/obs_model_4>


# The "singleton property" approach.  Note that this file does not fully
# implement singleton properties, since they rely on RDF triples outside of OWL
# and Manchester Syntax.  model_4.tll, originally generated from this file by
# Protege and with additional triples manually added, completes the
# implementation.
# The approach was first described here:
# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4350149/
# but Nguyen et al., "On Reasoning with RDF Statements about Statements using
# Singleton Property Triples", (https://arxiv.org/pdf/1509.04513.pdf) contains
# important implementation details.


ObjectProperty: obo:associated_with
ObjectProperty: obo:feeding_on
    SubPropertyOf: obo:associated_with
ObjectProperty: obo:ovipositing_on
    SubPropertyOf: obo:associated_with

ObjectProperty: obo:feeding_on_1
ObjectProperty: obo:feeding_on_2
ObjectProperty: obo:ovipositing_on_1


AnnotationProperty: obo:has_relation


Class: obo:Observation
Class: obo:Plant
Class: obo:Cicada
Class: obo:Wasp


Individual: obo:plant_1
    Types:
        obo:Plant


Individual: obo:cicada_1
    Types:
        obo:Cicada

    Facts:
        obo:feeding_on_1  obo:plant_1,
        obo:ovipositing_on_1  obo:plant_1


Individual: obo:wasp_1
    Types:
        obo:Wasp

    Facts:
        obo:feeding_on_2  obo:cicada_1


Individual: obo:observation_1
    Annotations:
        obo:has_relation  obo:feeding_on_1

    Types:
        obo:Observation


Individual: obo:observation_2
    Annotations:
        obo:has_relation  obo:ovipositing_on_1

    Types:
        obo:Observation


Individual: obo:observation_3
    Annotations:
        obo:has_relation  obo:feeding_on_2

    Types:
        obo:Observation

