
-- Example queries for using the use cases/comptency questions database.  The
-- two long queries at the end recreate the tables published in Supplemental 3
-- of Stucky et al. (2019) ________.


-- Get all user groups/domains and use cases.
SELECT u.user_group, c.use_case
  FROM user_groups AS u, use_cases AS c, use_cases_to_user_groups AS u2c
  WHERE u.id=u2c.user_group_id AND c.id=u2c.use_case_id ORDER BY u.display_order;

-- Put the results in a new table.
CREATE TABLE users_and_use_cases AS SELECT u.user_group, c.use_case
  FROM user_groups AS u, use_cases AS c, use_cases_to_user_groups AS u2c
  WHERE u.id=u2c.user_group_id AND c.id=u2c.use_case_id ORDER BY u.display_order;


-- Get all competency questions and their associated use cases.
SELECT q.question, c.use_case
  FROM questions AS q, use_cases AS c, questions_to_use_cases AS q2c
  WHERE q.id=q2c.question_id AND q2c.use_case_id=c.id;


-- Get all competency questions and their associated concept areas.
SELECT q.question, a.concept_area
  FROM questions AS q, concept_areas AS a, questions_to_concept_areas AS q2a
  WHERE q.id=q2a.question_id AND q2a.concept_area_id=a.id;


-- Get all competency questions and their associated use cases and concept
-- areas, each as a comma-separated list of numeric values.  Doubly nested
-- subselects are required to get the use cases and concept areas in sorted
-- order (there is no way to directly specify the order of group_concat()).
SELECT
  q.id, q.question, q.display_order,
  (
    SELECT group_concat(id, ',')
      FROM (
	SELECT c.id
          FROM use_cases AS c, questions_to_use_cases AS q2c
          WHERE q2c.question_id=q.id AND q2c.use_case_id=c.id
          ORDER BY c.id
      )
  ) use_cases_str,
  (
    SELECT group_concat(id, ',')
      FROM (
        SELECT a.id
          FROM concept_areas AS a, questions_to_concept_areas AS q2a
          WHERE q2a.question_id=q.id AND q2a.concept_area_id=a.id
          ORDER BY a.id
      )
  ) concept_areas_str
  FROM questions AS q
  WHERE q.publish=1
  ORDER BY q.display_order;


-- Get all user domains, use cases, and their associated competency questions
-- as a comma-separated list of numeric values.
SELECT
  u.user_group, c.use_case,
  (
    SELECT group_concat(qnum, ',')
      FROM (
        SELECT q.display_order AS qnum
          FROM questions AS q, questions_to_use_cases AS q2c
          WHERE q2c.use_case_id=c.id AND q2c.question_id=q.id AND q.publish=1
          ORDER BY qnum
      )
    ) questions_str
  FROM user_groups AS u, use_cases AS c, use_cases_to_user_groups AS c2u
  WHERE u.id=c2u.user_group_id AND c2u.use_case_id=c.id
  ORDER BY u.display_order, c.use_case;

