#!/usr/bin/python3

# Creates a markdown or HTML version of the example data.

import sqlite3
import html
from argparse import ArgumentParser


class BasePrinter:
    top_heading_str = 'Example Insect Natural History Data'
    preamble_text = 'These data were assembled by participants of a workshop held at the University of Florida from May 30 to June 1 of 2018.  The data cover all five major insect orders (Coleoptera, Diptera, Hemiptera, Hymenoptera, Lepidoptera) and represent most of the various kinds of natural history information found on insect specimen labels.  The data also include representative natural history information from literature sources and online databases.  For more information about how these data were assembled and why, see Stucky et al. (2019) __________.  Except for works in the public domain, data use licenses are as specified by the original data owners.'


class MarkdownPrinter(BasePrinter):
    def printPreamble(self):
        print('# ' + self.top_heading_str + '\n')
        print(self.preamble_text)

    def printOrderHeading(self, text):
        print('\n## {0}'.format(text))

    def printExample(
        self, example_num, order, family, sci_name, metadata, content_label,
        content
    ):
        print('\n### Example {0}'.format(example_num))

        taxon_str = order + ': ' + family
        if sci_name is not None:
            parts = sci_name.split()
            if parts[1] == 'sp.':
                taxon_str += ': _{0}_ sp.'.format(parts[0])
            else:
                taxon_str += ': _{0}_'.format(sci_name)

        print('**Taxonomy:** ' + taxon_str)

        for entry in metadata:
            print('**' + entry[0] + ':** ' + entry[1])

        print('**' + content_label + ':** `' + content + '`')

    def printReferenceHeading(self):
        print('## References')

    def printReference(self, ref_str, doi, url):
        ref_str = ref_str.replace('<i>', '_')
        ref_str = ref_str.replace('</i>', '_')

        if doi is not None:
            ref_str += ' DOI: ' + doi
        elif url is not None:
            ref_str += ' ' + url

        print('1. ' + ref_str)

    def printReferencesEnding(self):
        pass


class HTMLPrinter(BasePrinter):
    def printPreamble(self):
        print("""<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Example Insect Natural History Data</title>
<meta content="text" charset="utf-8" />
<link href="style/example_data_styles.css" rel="stylesheet" type="text/css" />
</head>""")

        print('<h1>' + self.top_heading_str + '</h1>')
        print('<p>' + self.preamble_text + '</p>')

    def printOrderHeading(self, text):
        print('<h2 class="order_name">{0}</h2>'.format(text))

    def printExample(
        self, example_num, order, family, sci_name, metadata, content_label,
        content
    ):
        print('<div class="data_example">')
        print('<h3>Example {0}</h3>'.format(example_num))

        taxon_str = order + ': ' + family
        if sci_name is not None:
            parts = sci_name.split()
            if parts[1] == 'sp.':
                taxon_str += ': <span class="sci_name">{0}</span> sp.'.format(parts[0])
            else:
                taxon_str += ': <span class="sci_name">{0}</span>'.format(sci_name)

        print('<ul>')
        print('<li><span class="metalabel">Taxonomy:</span> {0}</li>'.format(taxon_str))

        for entry in metadata:
            if entry[1].startswith('http://') or entry[1].startswith('https://'):
                print(
                    '<li><span class="metalabel">{0}:</span> <a href="{1}">{1}</a></li>'.format(
                        entry[0], entry[1]
                    )
                )
            else:
                print(
                    '<li><span class="metalabel">{0}:</span> {1}</li>'.format(
                        entry[0], entry[1]
                    )
                )

        content = html.escape(content)
        content = content.replace('\n', '</p><p>')
        content = content.encode('ascii', 'xmlcharrefreplace').decode()
        print(
            '<li><span class="metalabel">{0}:</span> <div class="ex_content"><p>{1}</p></div></li>'.format(
                content_label, content
            )
        )

        print('</ul>\n</div>')

    def printReferenceHeading(self):
        print('<h2 class="references">References</h2>\n<ul class="references">')

    def printReference(self, ref_str, doi, url):
        if doi is not None:
            ref_str += ' DOI: <a href="https://doi.org/{0}">{0}</a>'.format(doi)
        elif url is not None:
            ref_str += ' <a href="{0}">{0}</a>'.format(url)
        print('<li>{0}</li>'.format(ref_str))

    def printReferencesEnding(self):
        print('</ul>\n</html>')


argp = ArgumentParser(
    description='Generates a markdown or HTML version of the example data.'
)
argp.add_argument(
    '-f', '--format', type=str, required=False, default='markdown',
    choices=['markdown', 'html'], help='The output format (default="markdown").'
)

args = argp.parse_args()

conn1 = sqlite3.connect('example_data.sqlite')
conn1.row_factory = sqlite3.Row
c1 = conn1.cursor()

c1.execute('select e.exampleID, e.`order`, e.family, e.scientific_name, e.record_type, e.life_stage, e.record_URL, e.image_URL, e.content, s.short_citation as source from examples e, sources s where e.sourceID=s.sourceID order by e.exampleID;')
rows1 = c1.fetchall()

if args.format == 'markdown':
    p = MarkdownPrinter()
elif args.format == 'html':
    p = HTMLPrinter()

p.printPreamble()

ordername = ''
for row1 in rows1:
    if row1['order'] != ordername:
        ordername = row1['order']
        p.printOrderHeading(ordername)

    metadata = []
    metadata.append(('Record type', row1['record_type']))

    if row1['life_stage'] is not None:
        metadata.append(('Life stage(s)', row1['life_stage']))

    metadata.append(('Source', row1['source']))

    if row1['record_URL'] is not None:
        metadata.append(('Record URL', row1['record_URL']))

    if row1['image_URL'] is not None:
        # All records with images are either specimens or Wikimedia Commons
        # field photographs.
        if row1['record_type'] == 'specimen':
            metadata.append(('Label image', row1['image_URL']))
        else:
            metadata.append(('Photograph', row1['image_URL']))

    content = row1['content']
    content_label = ''

    # Generate an appropriate content label given the record type and contents.
    if row1['record_type'] == 'literature':
        content_label = 'Relevant text'
    elif row1['record_type'] == 'specimen':
        if content[0] == '"' and content[-1] == '"':
            content_label = 'Relevant label text'
        elif content.find('"') == -1:
            content_label = 'Comments'
        elif content[-1] == ')' and not(content[-2].isupper()):
            content_label = 'Relevant text and iDigBio data field(s)'
        else:
            content_label = 'Comments and relevant label text'
    elif row1['record_type'] == 'photograph':
        content_label = 'Description'
    else:
        content_label = 'Comments and relevant content'

    p.printExample(
        row1['exampleID'], row1['order'], row1['family'],
        row1['scientific_name'], metadata, content_label, content
    )

# Print the references.
p.printReferenceHeading()

c1.execute('select full_citation, URL, DOI from sources order by lower(full_citation);')
rows1 = c1.fetchall()

for row1 in rows1:
    p.printReference(row1['full_citation'], row1['DOI'], row1['URL'])

p.printReferencesEnding()

