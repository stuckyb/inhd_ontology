#!/usr/bin/python3

# Converts example data from a text file into a standard CSV format.
#
# Example input format for iDigBio specimen record:
#   Example 19
#   Taxonomy: Diptera: Asilidae
#   Life stage: larva
#   Specimen record: https://www.idigbio.org/portal/records/f21d9e25-ccb6-493e-81fc-c3dfeafd95de
#   Label image: https://invertnet.org/imagestor/vials/larges/2014/11/13/11211-0.jpg
#   Relevant label text: “ex: in wood – under bark of large elm log (not in contact w/ soil)”
#   
# Example input format for collection specimen without a digital record:
#   Example 40
#   Taxonomy: Diptera: Sarcophagidae: Acanthodotheca exuberans
#   Life stage: adult
#   Record Type: specimen
#   Source: Canadian National Collection of Insects, Arachnids, and Nematodes (CNC)
#   Relevant label text: “reared from Melanoplus mexicanus | newly dead and dying M. m. mexicanus | pupae 4th Aug. | flies 22-25 Aug.”
#   
# Example input format for a literature record:
#   Example 66
#   Taxonomy: Diptera: Sarcophagidae: Blaesoxipha impar
#   Life stage: larva
#   Publication: Roback (1954)
#   Relevant text: “has been reared from garbage by the author”
#   
# Example input format for a database:
#   Example
#   Taxonomy: Diptera: Calliphoridae: 
#   Life stage: adult
#   Database: West Nile Virus Mosquito Test Results from the City of Chicago
#   Content: Records the numbers of mosquitoes found in traps in Chicago between 2007 and 2017 and whether they were positive or negative for West Nile Virus.  Does not include absence data, i.e. every record has at least one mosquito.
#   
# Example input format for a photograph:
#   Taxonomy: Diptera: Asilidae: Triorla interrupta
#   Life stage: adult
#   Source: Wikidata
#   Photograph: https://commons.wikimedia.org/wiki/File:Robber_Fly_(Triorla_interrupta)_with_Dragonfly_(Plathemis_lydia).jpg
#   Content: Photograph of robber fly with prey, found by querying Wikidata for images of asilids carrying prey.  Both the robber fly and prey are identified to species.
#
# Note that all of the above are mapped to a common set of table columns, and
# this format can also be used for any record:
#   record_type: one of 'specimen', 'literature', 'database', 'photograph'
#   order: taxonomic order
#   family: taxonomic family
#   scientific_name: scientific name, if available
#   life_stage: the life stage of the observed insect
#   source: the name of the data source
#   URL: the URL of the record
#   image_URL: URL for an associated image
#   content: relevant content for the record
#


import os.path
import csv
from argparse import ArgumentParser


# Enumerate all field names that can be used in source text files.  Spaces can
# be used in place of underscores.
SRC_FIELD_NAMES = set([
    'taxonomy', 'specimen_record', 'label_image', 'publication', 'database',
    'photograph', 'relevant_label_text', 'relevant_text', 'content',
    'life_stage', 'url', 'source', 'record_type', 'source', 'image_url',
    'identifier', 'collection', ''
])


def processExample(ex_lines):
    """
    Processes the lines for a single example and returns a dictionary of data
    records for the example.
    """
    res = {}
    for line in ex_lines:
        parts = line.split(': ', 1)
        if len(parts) == 2:
            field, data = parts
        else:
            field = ''
            data = parts[0]

        # Identify and fix cases where a line is a continuation of a data value
        # and happens to contain a colon (':').
        if field.strip().lower().replace(' ', '_') not in SRC_FIELD_NAMES:
            data = field + ': ' + data
            field = ''

        field = field.strip().lower().replace(' ', '_')
        if field == 'url':
            field = 'URL'
        elif field == 'image_url':
            field = 'image_URL'

        if field == 'taxonomy':
            names = data.split(': ')
            res['order'] = names[0]
            res['family'] = names[1]
            if len(names) > 2:
                res['scientific_name'] = names[2]
        elif field == 'specimen_record':
            res['URL'] = data
            res['record_type'] = 'specimen'
            if 'idigbio.org' in data:
                res['source'] = 'iDigBio'
        elif field == 'publication':
            res['record_type'] = 'literature'
            res['source'] = data
        elif field == 'database':
            res['record_type'] = 'database'
            res['source'] = data
        elif field == 'photograph':
            res['record_type'] = 'photograph'
            res['image_URL'] = data
        elif field == 'label_image':
            res['image_URL'] = data
        elif field in ('relevant_label_text', 'relevant_text'):
            res['content'] = data
        elif field == '':
            res['content'] = res['content'] + '\n' + data
        else:
            res[field] = data

    return res


argp = ArgumentParser(
    description='Converts example data from a text file into a standard CSV '
    'format.'
)
argp.add_argument(
    '-o', '--output', type=str, required=False, default='', help='An output '
    'CSV file.  If no output file name is provided, the output file name will '
    'be derived from the input file name.'
)
argp.add_argument(
    'example_data_file', type=str, help='An an input text file of example data.'
)

args = argp.parse_args()

if args.output == '':
    args.output = os.path.splitext(args.example_data_file)[0] + '.csv'

if False:#os.path.exists(args.output):
    exit(
        '\nERROR: The output file, {0}, already exists.\n'.format(args.output)
    )

# A list to accumulate all parsed example data records.
ex_data = []

# Read the lines from the source file and bundle them into lists of lines that
# correspond with each example.
ex_lines = []
with open(args.example_data_file, encoding='utf-8-sig') as fin:
    for line in fin:
        line = line.strip()
        if line.startswith('Example'):
            if len(ex_lines) > 0:
                ex_data.append(processExample(ex_lines))
            ex_lines = []
        elif line != '':
            line = line.replace('“', '"').replace('”', '"')
            ex_lines.append(line)

if len(ex_lines) > 0:
    ex_data.append(processExample(ex_lines))

#print(ex_data)

# Calculate the set of all field names used for the example data.
fields = set()
for record in ex_data:
    fields.update(record.keys())

print('\nFields used for example data: {0}\n'.format(', '.join(fields)))

with open(args.output, 'w') as fout:
    writer = csv.DictWriter(fout, [
        'order', 'family', 'scientific_name', 'record_type', 'life_stage',
        'source', 'URL', 'image_URL', 'content', 'identifier', 'collection'
    ])
    writer.writeheader()

    for record in ex_data:
        writer.writerow(record)

