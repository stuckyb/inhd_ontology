#!/usr/bin/python

import argparse
import sys
import random
import re
import os


argp = argparse.ArgumentParser(description='Increments (or decrements) numerical values in one or more file names by a specified amount.  This is useful for automatically renaming files with names that include a numerical sequence that needs to be incremented or decremented while preserving file name order.  For example, rnaming "file_1.ext", "file_2.ext", "file_3.ext", etc. to "file_3.ext", "file_4.ext", "file_5.ext", etc.')
argp.add_argument('-i', '--increment', type=int, required=True, help='An integer to add to each integer value in the file names.')
argp.add_argument('-s', '--simulate', action='store_true', help='Output the file name changes without actually changing any file names.')
argp.add_argument('filename', nargs='+', help='A file name to convert.')

args = argp.parse_args()

fnames = args.filename

chars = [chr(val) for val in range(ord('a'), ord('z'))]

# Give each file a temporary name to avoid renaming collisions.
tmpnames = []
count = 0
for fname in fnames:
    count += 1
    tmpname = ''.join(random.sample(chars, 16))
    tmpname += '-' + str(count)

    tmpnames.append(tmpname)

    if not(args.simulate):
        os.rename(fname, tmpname)

# Generate the new name for and rename each file.
count = 0
for fname in fnames:
    parts = re.split('(\d+)', fname)
    newname = ''
    for part in parts:
        try:
            int(part)
            newname += str(int(part) + args.increment)
        except ValueError:
            newname += part

    if not(args.simulate):
        os.rename(tmpnames[count], newname)

    print '{0} => {1}'.format(fname, newname)

    count += 1

